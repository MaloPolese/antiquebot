module.exports = class ChangeRole {
  static roles = [];
  static users = [];

  constructor() {}

  start() {
    setInterval(() => {
      console.log(this.roles, this.users);
    }, 2_000);
  }

  addUser(user) {
    this.users.push(user);
    console.log(this.users);
  }

  addRole(role) {
    this.roles.push(role);
    console.log(this.roles);
  }

  removeRole(role) {
    this.roles.splice(this.roles.indexOf(role), 1);
  }

  removeUser(user) {
    this.users.splice(this.users.indexOf(user), 1);
  }
};
