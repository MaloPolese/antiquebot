module.exports = class NewUser {
  constructor(bot) {
    bot.on("guildMemberAdd", (member) => {
      this.addNewRole(member, "amis");
    });
  }
  addNewRole(member, startRole) {
    const role = member.guild.roles.cache.find(
      (role) => role.name === startRole
    );
    member.roles.add(role);
  }
};
