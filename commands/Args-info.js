module.exports = {
  name: "args-info",
  description: "Informations sur les arguments fournis.",
  args: true,
  usage: "<user> <role>",
  execute(message, args) {
    if (args[0] === "foo") {
      return message.channel.send("bar");
    }

    message.channel.send(`First argument: ${args[0]}`);
  },
};
