const { prefix } = require('../config.json');
const Discord = require('discord.js');

module.exports = {
  name: 'help',
  description: 'Listez toutes mes commandes ou des informations sur une commande spécifique.',
  aliases: ['commands'],
  usage: '<command name>',
  cooldown: 1,
  execute(message, args) {
    const data = [];
    const { commands } = message.client;
    const helpEmbed = new Discord.MessageEmbed()
      .setColor('#dc143c')
      .setTitle('Help Commandes');

    if (!args.length) {

      helpEmbed.setDescription('Voici une liste de toutes mes commandes :')
      commands.map(command => helpEmbed.addField(`**${command.name}**`, command.description));
      helpEmbed.setFooter(`\nVous pouvez envoyer \`${prefix}help <command name>\` pour obtenir des informations sur une commande spécifique !`)
      return message.author.send(helpEmbed)
        .then(() => {
          if (message.channel.type === 'dm') return;
          message.reply('Je vous ai envoyé un DM avec tous mes commandes !');
        })
        .catch(error => {
          console.error(`Impossible d'envoyer l'help en DP à ${message.author.tag}.\n`, error);
          message.reply('il semble que je ne puisse pas vous DM ! Avez-vous désactivé vos DM ?');
        });
    }
    const name = args[0].toLowerCase();
    const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

    if (!command) {
      return message.reply('La commande n\'est pas valide');
    }


    helpEmbed.addField('**Name:**', command.name);

    if (command.aliases) helpEmbed.addField('**Aliases:**', command.aliases.join(', '));
    if (command.description) helpEmbed.addField('**Description:**', command.description);
    if (command.usage) helpEmbed.addField('**Usage:**', `${prefix}${command.name} ${command.usage}`);
    helpEmbed.addField('**Cooldown:**', `${command.cooldown || 3} second(s)`);

    message.channel.send(helpEmbed);

  },
};