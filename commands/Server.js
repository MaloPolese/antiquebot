const Discord = require('discord.js');
module.exports = {
  name: "server",
  description: "Affiche des informations sur le serveur",
  guildOnly: true,
  execute(message) {
    const serverEmbed = new Discord.MessageEmbed()
      .setColor('#dc143c')
      .setTitle('Server')
      .addField(`**Nom du serveur**`, message.guild.name)
      .addField(`**Nombre de personnes**`, message.guild.memberCount);

    message.channel.send(serverEmbed);
  },
};
