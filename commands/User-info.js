const Discord = require('discord.js');

module.exports = {
  name: "user-info",
  description: "Affiche tes informations",
  aliases: ['uInfo', 'user'],
  execute(message) {
    const userEmbed = new Discord.MessageEmbed()
      .setColor('#dc143c')
      .setTitle('Utilisateur')
      .addField(`**Ton nom**`, message.author.username)
      .addField(`**Ton ID**`, message.author.id);
    message.channel.send(userEmbed);
  },
};
