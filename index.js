const config = require("./config.json");
const fs = require("fs");
const Discord = require("discord.js");
const client = new Discord.Client();
const cooldowns = new Discord.Collection();

client.once("ready", () => {
  client.user.setActivity(config.activity).catch(console.error);
  console.log(config.initMsg, '\n' + new Date().toString());
});

/*****************
 * Init Commands *
 *****************/
client.commands = new Discord.Collection();
const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));
console.log(`${commandFiles.length} command(s) chargé`);

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

client.on("message", (message) => {
  if (!message.content.startsWith(config.prefix) || message.author.bot) return;

  const args = message.content.slice(config.prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();

  const command = client.commands.get(commandName)
    || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

  if (!command) return;

  if (command.args && !args.length) {
    let reply = `Vous n'avez fourni aucun argument, ${message.author}!`;

    if (command.usage) {
      reply += `\nL'usage approprié serait: \`${prefix}${command.name} ${command.usage}\``;
    }
    return message.channel.send(reply);
  }

  if (command.guildOnly && message.channel.type !== "text") {
    return message.reply(
      "Je ne peux pas exécuter cette commande à l'intérieur des DM !"
    );
  }

  if (!cooldowns.has(commandName)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || config.defaultCooldown) * 1000;

  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;
      return message.reply(
        `Veuillez patienter ${timeLeft.toFixed(1)} seconde(s) avant de réutiliser \`${command.name}\` commande.`
      );
    }
  }
  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  try {
    command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply("Il y a eu une erreur en essayant d'exécuter cette commande !");
  }
});

/***************
 * Init Events *
 ***************/
fs.readdir("./events/", (err, files) => {
  if (err) console.log(err);

  files.forEach((f) => {
    const e = require(`./events/${f}`);
  });
  console.log(`${files.length} event(s) chargé`);
});

/**************
 * Init Login *
 **************/
if (process.env.TOKEN) {
  client.login(process.env.TOKEN);
} else {
  const token = require("./token.json");
  client.login(token.token);
}
